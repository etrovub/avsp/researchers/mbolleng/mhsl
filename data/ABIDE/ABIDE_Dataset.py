from torch.utils.data import Dataset
from utils.data import *
import pandas as pd
import numpy as np
import dgl
#from data.ABIDE.phenotyic_processing import preprocess
from tqdm import tqdm
import warnings
warnings.filterwarnings("ignore")


class ABIDE_Dataset(Dataset):
    def __init__(self, fully_connected=True, k_split=10, setting='inductive'):
        super(Dataset, self).__init__()
        self.setting = setting

        self.nbr_graphs = k_split

        self.graph_lists = []
        self.label_lists = []
        self.fully_connected = fully_connected
        self._prepare()

    def _prepare(self):

        data = pd.read_csv("C:/Users/maxim/PycharmProjects/PhD_HyperGraph/data/ABIDE/experimental_data_MMGL.csv").values
        modal_feat_dict = np.load("C:/Users/maxim/PycharmProjects/PhD_HyperGraph/data/ABIDE/modal_feat_dict.npy", allow_pickle=True).item()

        input_data_dims = []
        self.modal_name = modal_feat_dict.keys()
        for i in self.modal_name:
            input_data_dims.append(len(modal_feat_dict[i]))
        print('Modal dims ', input_data_dims)
        self.modal_dim = input_data_dims
        input_data = data[:, :-1]
        self.n_patient = data.shape[0]
        label = data[:, -1] - 1
        skf = StratifiedKFold(n_splits=self.nbr_graphs, random_state=42, shuffle=True)
        idx = skf.split(input_data, label)
        print('Preparing graphs for {} setting'.format(self.setting))
        for fold in tqdm(idx, total=self.nbr_graphs, colour='green'):
            if self.setting == 'inductive':
                data = input_data[fold[1]]
                lab = label[fold[1]]
            else:
                data = input_data
                lab = label
            g = dgl.DGLGraph()
            g.add_nodes(data.shape[0])
            g.ndata['feat'] = torch.tensor(data).float()
            g.ndata['label'] = torch.tensor(lab)
            if self.setting == 'transductive':
                val_idx = torch.tensor(fold[1]).long()
                train_idx = torch.tensor(fold[0]).long()
                g.ndata['idx_val'] = torch.zeros((data.shape[0]))
                g.ndata['idx_train'] = torch.zeros((data.shape[0]))
                g.ndata['idx_val'][val_idx] = 1
                g.ndata['idx_train'][train_idx] = 1
            buff = 0
            for i in self.modal_name:
                n_modal = len(modal_feat_dict[i])
                g.ndata[i] = torch.tensor(data[:, buff:buff+n_modal]).float()
                buff = n_modal
            if self.fully_connected:
                A = np.ones((data.shape[0], data.shape[0]))
                edge = np.array(get_vertices(A)[0])
                edge_feat = np.array(get_vertices(A)[1])
                for src, dst in edge:
                    g.add_edges(src.item(), dst.item())
                edge_feat = np.array(edge_feat)
                g.edata['feat'] = torch.tensor(edge_feat).long()
            onehot = [0, 0]
            tmp = []
            for i in lab:
                onehot[int(i)] = 1
                tmp.append(onehot)
                onehot = [0, 0]
            self.label_lists.append(torch.tensor(tmp))
            self.graph_lists.append(g)

    def __len__(self):
        return self.nbr_graphs

    def __getitem__(self, idx):
        return self.graph_lists[idx], self.label_lists[idx]