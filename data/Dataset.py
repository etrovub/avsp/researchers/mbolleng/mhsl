import json
import pickle
import time
from torch.utils.data import DataLoader
import dgl

from utils.data import *


class Dataset(torch.utils.data.Dataset):
    def __init__(self, name, fully_connected=True, k_split=10, setting='inductive'):
        t0 = time.time()
        self.name = name
        self.setting =setting
        self.k_split = k_split
        self.fc = 'fc' if fully_connected else 'nc'
        path = os.getcwd() + '/data/{}/'.format(name)
        save_dir = path + 'save/'
        if not (os.path.exists(save_dir + 'graph_{}_{}_{}_{}.pkl'.format(name, setting, self.k_split, self.fc))):

            if name == 'TADPOLE':
                from data.TADPOLE.TADPOLE_Dataset import TADPOLE_Dataset
                self.n_class = 3
                self.n_modal = 6
                dataset = TADPOLE_Dataset(k_split=k_split, fully_connected=fully_connected, setting=setting)
                self.modal_name = list(dataset.modal_name)
                self.modal_dim = dataset.modal_dim
                self.n_patient = dataset.n_patient
            elif name == 'ABIDE':
                from data.ABIDE.ABIDE_Dataset import ABIDE_Dataset
                self.n_class = 2
                self.n_modal = 4
                dataset = ABIDE_Dataset(fully_connected=fully_connected, k_split=k_split, setting=setting)
                self.modal_name = list(dataset.modal_name)
                self.modal_dim = dataset.modal_dim
                self.n_patient = dataset.n_patient
            else:
                raise NotImplementedError

            print("[!] Dataset: ", self.name)

            self.all_idx = get_all_split_idx(dataset, path, k_split, setting)
            self.all = dataset
            self.train = [self.format_dataset([dataset[idx] for idx in self.all_idx['train'][split_num]]) for split_num
                          in
                          range(k_split)].copy()
            self.val = [self.format_dataset([dataset[idx] for idx in self.all_idx['val'][split_num]]) for split_num in
                        range(k_split)].copy()
            self.test = [self.format_dataset([dataset[idx] for idx in self.all_idx['test'][split_num]]) for split_num in
                         range(k_split)].copy()
            self.save(save_dir)
        else:
            with open(save_dir + 'graph_{}_{}_{}_{}.pkl'.format(name, self.setting, self.k_split, self.fc), "rb") as f:
                f = pickle.load(f)
                self.train = f[0]
                self.val = f[1]
                self.test = f[2]
            with open(save_dir + 'info_{}_{}_{}_{}.json'.format(name, self.setting, self.k_split, self.fc), "rb") as fp:
                info = json.load(fp)
                self.n_class = info['n_class']
                self.n_modal = info['n_modal']
                self.modal_name = info['modal_name']
                self.modal_dim = info['modal_dim']
                self.n_patient = info['n_patient']
        print("Time taken: {:.4f}s".format(time.time() - t0))

    def save(self, save_dir):
        start = time.time()
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
        with open(save_dir + 'graph_{}_{}_{}_{}.pkl'.format(self.name, self.setting, self.k_split, self.fc), 'wb') as f:
            pickle.dump([self.train, self.val, self.test], f)
        info = {'n_class': self.n_class, 'n_modal': self.n_modal, 'modal_dim': self.modal_dim, 'modal_name': self.modal_name, 'n_patient': self.n_patient}
        with open(save_dir + 'info_{}_{}_{}_{}.json'.format(self.name, self.setting, self.k_split, self.fc), "w") as fp:
            json.dump(info, fp)
        print(' data saved : Time (sec):', time.time() - start)


    def format_dataset(self, dataset):
        """
            Utility function to recover data,
            INTO-> dgl/pytorch compatible format
        """
        graphs = [data[0] for data in dataset]
        labels = [data[1] for data in dataset]

        return DGLFormDataset(graphs, labels)

    # form a mini batch from a given list of samples = [(graph, label) pairs]
    def collate(self, samples):
        # The input samples is a list of pairs (graph, label).
        graphs, labels = map(list, zip(*samples))
        labels = torch.cat(labels).long()
        batched_graph = dgl.batch(graphs)
        return batched_graph, labels