
from sklearn.metrics import roc_curve, auc
import numpy as np


def compute_roc_auc(labels, prediction):
    fpr, tpr, thresholds = roc_curve(labels, prediction, pos_label=1)
    roc_auc = auc(fpr, tpr)
    return roc_auc


def accuracy(labels, prediction):
    acc = np.sum(prediction == labels)/labels.shape[0]
    return acc

