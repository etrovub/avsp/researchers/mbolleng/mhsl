import os
import random
import torch
import numpy as np
import yaml


def gpu_setup(gpu_config, gpu_id=None):
    use_gpu = gpu_config['use']
    if gpu_id is None:
        gpu_id = gpu_config['id']
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu_id)

    if torch.cuda.is_available() and use_gpu:
        print('cuda available with GPU:', torch.cuda.get_device_name(0))
        device = torch.device("cuda")
    else:
        print('cuda not available')
        device = torch.device("cpu")
    return device


def view_model_param(model):
    total_param = 0
    print("MODEL DETAILS:\n")
    # print(model)
    for param in model.parameters():
        # print(param.data.size())
        total_param += np.prod(list(param.data.size()))
    print('Total parameters:', total_param)
    return total_param


def load_config(config_path):
    with open(config_path) as file:
        config = yaml.safe_load(file)
    return config


def merge(dict1, dict2):
    return dict2.update(dict1)


def seed_everything(seed, device):
    random.seed(seed)
    # os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if device != 'cpu':
        torch.cuda.manual_seed(seed)
        """torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = True"""
