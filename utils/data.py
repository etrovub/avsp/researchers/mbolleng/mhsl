import csv
import os
import torch
from sklearn.model_selection import StratifiedKFold, train_test_split
import numpy as np
class DGLFormDataset(torch.utils.data.Dataset):
    """
        DGLFormDataset wrapping graph list and label list as per pytorch Dataset.
        *lists (list): lists of 'graphs' and 'labels' with same len().
    """

    def __init__(self, *lists):
        assert all(len(lists[0]) == len(li) for li in lists)
        self.lists = lists
        self.graph_lists = lists[0]
        self.label_lists = lists[1]

    def __getitem__(self, index):
        return tuple(li[index] for li in self.lists)

    def __len__(self):
        return len(self.lists[0])


def format_dataset(dataset):
    """
        Utility function to recover data,
        INTO-> dgl/pytorch compatible format
    """
    graphs = [data[0] for data in dataset]
    labels = [data[1] for data in dataset]

    for graph in graphs:
        graph.ndata['feat'] = graph.ndata['feat'].float()
        if 'feat' not in graph.edata.keys():
            edge_feat_dim = graph.ndata['feat'].shape[1]  # dim same as node feature dim
            graph.edata['feat'] = torch.ones(graph.number_of_edges(), edge_feat_dim)

    return DGLFormDataset(graphs, labels)


def get_vertices(a):
    edges = []
    feat = []
    for i in range(a.shape[1]):
        for j in range(a.shape[0]):
            if a[i, j] != 0:
                edges.append((i, j))
                feat.append(a[i, j])
    return edges, feat


def get_all_split_idx(dataset, path, k_splits, setting):
    """
        - Split total number of graphs into 3 (train, val and test) in 3:1:1
        - Stratified split proportionate to original distribution of data with respect to classes
        - Using sklearn to perform the split and then save the indexes
        - Preparing 5 such combinations of indexes split to be used in Graph NNs
        - As with KFold, each of the 5 fold have unique test set.
    """
    root_idx_dir = '{}/split/'.format(path)
    if not os.path.exists(root_idx_dir):
        os.makedirs(root_idx_dir)
    all_idx = {}

    # If there are no idx files, do the split and store the files
    if not (os.path.exists(root_idx_dir + '_train_{}_{}.index'.format(setting, k_splits))):
        if setting == 'inductive':
            print("[!] Splitting the data into train/val/test ...")
            for i in range(len(dataset.graph_lists)):
                dataset[i][0].a = lambda: None
                setattr(dataset[i][0].a, 'index', i)
            """remain_set, test, _, _ = train_test_split(dataset,
                                                           np.arange(len(dataset.graph_lists)),
                                                           test_size=1/(k_splits), random_state=42)"""
            """nbr_test = len(dataset.graph_lists)//k_splits
            buff = len(dataset.graph_lists)-nbr_test
            remain_set = dataset[0:buff][0]
            test_index = list(np.arange(buff, len(dataset.graph_lists)))"""

            cross_val_fold = StratifiedKFold(n_splits=k_splits, shuffle=True, random_state=42)

            # this is a temporary index assignment, to be used below for val splitting
            remain_set = dataset[:][0]

            label_lst = [1 for _ in range(len(remain_set))]

            for indexes in cross_val_fold.split(remain_set, label_lst):

                train_index, val_index = indexes[0], indexes[1]

                train = format_dataset([dataset[index] for index in train_index])
                val = format_dataset([dataset[index] for index in val_index])
                test = format_dataset([dataset[index] for index in val_index])

                train, val, test = format_dataset(train), format_dataset(val), format_dataset(test)


                # Extracting only idxs
                idx_train = [int(item[0].a.index) for item in train]
                idx_val = [int(item[0].a.index) for item in val]
                idx_test = [int(item[0].a.index) for item in test]

                f_train = open(root_idx_dir + '_train_{}_{}.index'.format(setting, k_splits), 'a+', newline='')
                f_val = open(root_idx_dir + '_val_{}_{}.index'.format(setting, k_splits), 'a+', newline='')
                f_test = open(root_idx_dir + '_test_{}_{}.index'.format(setting, k_splits), 'a+', newline='')

                f_train_w = csv.writer(f_train)
                f_val_w = csv.writer(f_val)
                f_test_w = csv.writer(f_test)

                f_train_w.writerow(idx_train)
                f_val_w.writerow(idx_val)
                f_test_w.writerow(idx_test)

                f_train.close()
                f_val.close()
                f_test.close()
        else:
            for i in range(k_splits):
                # Extracting only idxs
                idx_train = [i]
                idx_val = [i]
                idx_test = [i]

                f_train = open(root_idx_dir + '_train_{}_{}.index'.format(setting, k_splits), 'a+', newline='')
                f_val = open(root_idx_dir + '_val_{}_{}.index'.format(setting, k_splits), 'a+', newline='')
                f_test = open(root_idx_dir + '_test_{}_{}.index'.format(setting, k_splits), 'a+', newline='')

                f_train_w = csv.writer(f_train)
                f_val_w = csv.writer(f_val)
                f_test_w = csv.writer(f_test)

                f_train_w.writerow(idx_train)
                f_val_w.writerow(idx_val)
                f_test_w.writerow(idx_test)

                f_train.close()
                f_val.close()
                f_test.close()
    print("[!] Splitting done!")
    # reading idx from the files
    for section in ['train', 'val', 'test']:
        with open(root_idx_dir + '_' + section + '_{}_{}.index'.format(setting, k_splits), 'r') as f:
            reader = csv.reader(f)
            all_idx[section] = [list(map(int, idx)) for idx in reader]
            f.close()
    return all_idx
