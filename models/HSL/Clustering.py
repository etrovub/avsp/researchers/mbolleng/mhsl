import torch.nn as nn
import torch
from typing import Optional


class Clustering(nn.Module):
    def __init__(self, n_clusters, input_shape, alpha=1, cluster_centers: Optional[torch.Tensor] = None):
        super().__init__()
        self.n_clusters = n_clusters
        self.alpha = alpha
        self.input_shape = input_shape
        if cluster_centers is None:
            initial_cluster_centers = torch.zeros(self.n_clusters, self.input_shape, dtype=torch.float32)
            nn.init.xavier_uniform_(initial_cluster_centers)
        else:
            initial_cluster_centers = cluster_centers
        self.clustcenters = nn.Parameter(initial_cluster_centers)

    def forward(self, inputs):
        """ student t-distribution, as same as used in t-SNE algorithm.
            inputs: the variable containing data, shape=(n_samples, n_features)
            output: student's t-distribution, or soft labels for each sample. shape=(n_samples, n_clusters)
        """
        q = 1.0 / (1.0 + (torch.sum(torch.square(torch.unsqueeze(inputs, dim=1) - self.clustcenters), dim=2) / self.alpha))
        q **= (self.alpha + 1.0) / 2.0
        q = torch.transpose(torch.transpose(q, 0, 1) / torch.sum(q, dim=1), 0, 1)
        return q
