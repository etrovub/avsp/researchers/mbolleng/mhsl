import torch
import torch.nn as nn
import numpy as np
import torch.nn.functional as f
from models.HSL.MLP import MLP
from models.HSL.Clustering import Clustering
from models.HSL.HGL import HGL
from einops import rearrange


class HSL(nn.Module):
    def __init__(self, net_params):
        super().__init__()
        self.n_class = int(net_params['n_class'])
        self.n_modal = int(net_params['n_modal'])
        self.modal_name = net_params['modal_name']
        self.modal_dim = net_params['modal_dim']
        self.n_layer = net_params['n_layer']
        self.net_params = net_params
        self.device = net_params['device']
        self.n_cluster = net_params['n_cluster']
        self.n_view = net_params['n_view']
        self.fuse_mode = net_params['fuse']
        self.att = net_params['att']
        self.batch_norm = net_params['batch_norm']
        self.residual = net_params['residual']

        self.in_dim = int(np.sum(self.modal_dim))
        self.hidden_dim = net_params['hidden_dim']
        self.embed = nn.Linear(self.in_dim, self.hidden_dim)
        self.embed1 = nn.Linear(self.in_dim, self.hidden_dim)

        self.cluster_var = Clustering(self.n_view, net_params['n_patient'])
        self.clustering = Clustering(self.n_cluster, self.hidden_dim)
        self.HGL = nn.ModuleList([nn.ModuleList(
            [HGL(self.hidden_dim, self.hidden_dim, dropout=net_params['dropout'], device=self.device, att=self.att,
                 residual=self.residual, batch_norm=self.batch_norm) for _ in
             range(self.n_layer)]) for _ in range(self.n_view)])

        if self.fuse_mode == 'attention':
            self.hyperedge_proj = nn.ModuleList([nn.Linear(self.hidden_dim, self.hidden_dim) for _ in range(self.n_view)])
            self.node_proj = nn.Linear(self.hidden_dim, self.hidden_dim)
        elif self.fuse_mode == 'weighted_full':
            weight = torch.zeros((self.n_view, self.hidden_dim), dtype=torch.float32)
            nn.init.xavier_uniform_(weight)
            self.weight = nn.Parameter(weight)
        elif self.fuse_mode == 'weighted_short':
            weight = torch.zeros((self.n_view, 1), dtype=torch.float32)
            nn.init.xavier_uniform_(weight)
            self.weight = nn.Parameter(weight)

        self.MLP = MLP(self.hidden_dim, self.n_class, L=2)

        self.in_feat_dropout = nn.Dropout(net_params['in_feat_dropout'])
        self.dropout = nn.Dropout(net_params['dropout'])

        self.info = {}

    def multi_view(self, x):
        x_var = self.cluster_var(x.t())
        self.view = x_var
        for i in range(self.n_view):
            if i == 0:
                H = rearrange(self.clustering(self.dropout(self.embed(x * x_var[:, i]))), 'b n -> b 1 n')
                x_view = rearrange(self.dropout(self.embed(x * x_var[:, i])), 'b n -> b 1 n')
            else:
                H = torch.cat((H, rearrange(self.clustering(self.dropout(self.embed(x * x_var[:, i]))), 'b n -> b 1 n')), dim=1)
                x_view = torch.cat((x_view, rearrange(self.dropout(self.embed(x * x_var[:, i])), 'b n -> b 1 n')), dim=1)
        return H, x_view

    def multi_view_l(self, x):
        x_var = self.cluster_var(x.t())
        self.view = x_var
        for i in range(self.n_view):
            if i == 0:
                H = rearrange(self.clustering(self.dropout(x * x_var[:, i])), 'b n -> b 1 n')
                x_view = rearrange(self.dropout(x * x_var[:, i]), 'b n -> b 1 n')
            else:
                H = torch.cat((H, rearrange(self.clustering(self.dropout(x * x_var[:, i])), 'b n -> b 1 n')), dim=1)
                x_view = torch.cat((x_view, rearrange(self.dropout(x * x_var[:, i]), 'b n -> b 1 n')), dim=1)
        return H, x_view

    def fuse(self, mv, mve, H):
        if self.fuse_mode == 'mean':
            return torch.mean(mv, dim=1)
        elif self.fuse_mode == 'attention':
            for v in range(self.n_view):
                if v==0:
                    w = torch.unsqueeze(
                        self.node_proj(mv[:, v, :]) * torch.sum(torch.tanh(self.hyperedge_proj[v](torch.matmul(H[:, v, :], mve[:, v, :]))),
                                                                dim=0), dim=1)
                else:
                    w = torch.cat((w, torch.unsqueeze(
                        self.node_proj(mv[:, v, :]) * torch.sum(torch.tanh(self.hyperedge_proj[v](torch.matmul(H[:, v, :], mve[:, v, :]))),
                                                                    dim=0), dim=1)), dim=1)
            w = torch.softmax(w, dim=1)
            h = torch.sum(mv*w, dim=1)
            return h

        elif self.fuse_mode == 'weighted_full':
            return torch.sum(mv*torch.softmax(self.weight, dim=0), dim=1)

        elif self.fuse_mode == 'weighted_short':
            return torch.sum(mv*torch.softmax(self.weight, dim=0), dim=1)

    def forward(self, g):
        self.g = g
        feat = self.in_feat_dropout(g.ndata['feat'])
        self.feat = feat
        for l in range(self.n_layer):
            if l ==0:
                H, x_view = self.multi_view(feat)
            else:
                H, x_view = self.multi_view_l(x)
            self.H = H
            for v in range(self.n_view):
                if l==0:
                    x = self.embed1(feat)
                x, e = self.HGL[v][l](x, H[:, v])
                if v == 0:
                    mv = rearrange(x, 'b n -> b 1 n')
                    mve = rearrange(e, 'e n -> e 1 n')
                else:
                    mv = torch.cat((mv, rearrange(x, 'b n -> b 1 n')), dim=1)
                    mve = torch.cat((mve, rearrange(e, 'e n -> e 1 n')), dim=1)
            x = self.fuse(mv, mve, H)
        self.pred = self.MLP(x)
        return self.pred

    def loss(self, setting, mode, epoch):
        if setting == 'inductive':
            score = self.pred
            label = f.one_hot(self.g.ndata['label'].long())
            idx = torch.arange(0, score.shape[0])
        else:
            if mode == 'train':
                idx = torch.where(self.g.ndata['idx_train'] == 1)[0]
            else:
                idx = torch.where(self.g.ndata['idx_val'] == 1)[0]
            label = f.one_hot(self.g.ndata['label'][idx].long())
            score = self.pred[idx]


        V = label.size(0)
        label_count = torch.bincount(label.long().argmax(dim=1))
        label_count = label_count[torch.nonzero(label_count, as_tuple=False)].squeeze()
        cluster_sizes = label_count
        weight = (V - cluster_sizes).float() / V
        weight *= (cluster_sizes > 0).float()
        criterion = torch.nn.CrossEntropyLoss(weight=weight.to(
            self.device))
        class_loss = criterion(score.float(), label.float())

        self.info['class_loss'] = class_loss.detach().item()
        loss = class_loss
        return loss, None

    def accuracy(self, setting, mode):
        if setting == 'inductive':
            score = self.pred
            label = f.one_hot(self.g.ndata['label'].long())
        else:
            if mode == 'train':
                idx = torch.where(self.g.ndata['idx_train'] == 1)[0]
            else:
                idx = torch.where(self.g.ndata['idx_val'] == 1)[0]
            label = f.one_hot(self.g.ndata['label'][idx].long())
            score = self.pred[idx]
        score = torch.argmax(score, dim=1)
        target = torch.argmax(label, dim=1)
        acc = torch.sum(score == target) / score.shape[0]
        return acc



