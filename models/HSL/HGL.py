import dgl.sparse as dglsp
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch_scatter import scatter




class HGL(nn.Module):
    def __init__(self, in_size, out_size, dropout=0, device='cuda:0', att='none', batch_norm=None, residual=True):
        super().__init__()
        self.device = device
        self.in_dim= in_size
        self.W1 = nn.Linear(in_size, out_size)
        self.dropout = nn.Dropout(dropout)
        self.residual = residual
        self.batch_norm =batch_norm
        if batch_norm:
            self.BN = nn.BatchNorm1d(out_size)
        self.att = att

        if self.att == 'cos':
            self.node_proj = nn.Linear(in_size, out_size)
            self.edge_proj = nn.Linear(in_size, out_size)
        elif self.att == 'cat':
            self.node_proj = nn.Linear(in_size, out_size)
            self.edge_proj = nn.Linear(in_size, out_size)
            self.cat_proj = nn.Linear(2*in_size, 1)





    def forward(self, X, H):
        x_in = X
        # Compute node degree.
        d_V = H.sum(1)
        # Compute edge degree.
        d_E = H.sum(0)
        # Compute the inverse of the square root of the diagonal D_v.
        D_v_invsqrt = dglsp.diag(d_V ** -0.5)
        # Compute the inverse of the diagonal D_e.
        D_e_inv = dglsp.diag(d_E ** -1)
        D_e_invsqrt = dglsp.diag(d_E ** -0.5)
        # In our example, B is an identity matrix.
        n_edges = d_E.shape[0]
        B = dglsp.identity((n_edges, n_edges)).to(self.device)
        # Compute Laplacian from the equation above.

        E = dglsp.matmul(D_e_inv, H.t())
        E = dglsp.matmul(E, D_v_invsqrt.to_dense())
        E = dglsp.matmul(E, self.W1(self.dropout(X)))

        if self.att != 'none':
            e = self.edge_proj(torch.expand_copy(E, size=(H.shape[0], H.shape[1], self.in_dim)))
            v = self.node_proj(torch.expand_copy(torch.unsqueeze(x_in, dim=1), size=(H.shape[0], H.shape[1], self.in_dim)))
            if self.att == 'cos':
                sim = torch.cosine_similarity(v, e, dim=-1)
            elif self.att == 'cat':
                sim = F.leaky_relu(torch.squeeze(self.cat_proj(torch.cat((v, e), dim=-1)), dim=-1), 0.2)
            H = torch.softmax(sim, dim=1)

        X = dglsp.matmul(D_v_invsqrt, H)
        X = dglsp.matmul(X, E)

        X = torch.relu(X)
        if self.residual:
            X = X + x_in
        if self.batch_norm:
            X = self.BN(X)
        return X, E

    def get_compatible_hyper_edge(self, H):
        num_hyper_edge = H.size(1)
        hyper_edge_index = []
        hyper_edge_val = []
        for i in range(num_hyper_edge):
            for j in range(H.size(0)):
                if H[j, i] != 0:
                    hyper_edge_index.append([j, i])
                    hyper_edge_val.append(H[j, i])

        return torch.tensor(hyper_edge_index).long(), torch.tensor(hyper_edge_val).float()
