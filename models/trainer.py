import torch
import numpy as np
from utils.metrics import compute_roc_auc, accuracy


def accumulate(batch_scores, batch_labels, score_lst, label_lst):
    for i in range(len(batch_scores)):
        score_value = int(torch.argmax(batch_scores[i]).item())#float(batch_scores[i][1].item())
        lab_value = int(torch.argmax(batch_labels[i]).item())
        if len(score_lst) == 0:
            score_lst = np.expand_dims(np.array(score_value), axis=0)
            label_lst = np.expand_dims(np.array(lab_value), axis=0)
        else:
            score_lst = np.concatenate((score_lst, np.expand_dims(np.array(score_value), axis=0)),
                                             axis=0)
            label_lst = np.concatenate((label_lst, np.expand_dims(np.array(lab_value), axis=0)), axis=0)
    return score_lst, label_lst


def train_epoch(model, optimizer, device, data_loader, epoch, setting):
    model.train()
    epoch_loss = 0
    epoch_acc =0
    info = None
    score_lst = []
    label_lst = []
    for iter, (batch_graphs, batch_labels) in enumerate(data_loader):
        batch_graphs = batch_graphs.to(device)
        optimizer.zero_grad()
        model.forward(batch_graphs)
        loss, info = model.loss(setting, 'train', epoch)
        loss.backward()
        optimizer.step()
        epoch_loss += loss.detach().item()
        epoch_acc += model.accuracy(setting, 'train').detach().item()
    epoch_loss /= (iter + 1)
    epoch_acc /= (iter + 1)
    #epoch_acc = 0#accuracy(label_lst, score_lst)
    return epoch_loss, epoch_acc, optimizer, info, model


def evaluate_network(model, device, data_loader, epoch, setting):
    model.eval()
    epoch_loss = 0
    epoch_acc =0
    info = None
    score_lst = []
    label_lst = []
    with torch.no_grad():
        for iter, (batch_graphs, batch_labels) in enumerate(data_loader):
            if setting=='inductive':
                batch_graphs = batch_graphs.to(device)
                model.forward(batch_graphs)
            loss, info = model.loss(setting, 'val', epoch)
            epoch_loss += loss.detach().item()
            epoch_acc += model.accuracy(setting, 'val').detach().item()
        epoch_loss /= (iter + 1)
        epoch_acc /= (iter + 1)#epoch_acc = 0#accuracy(label_lst, score_lst)
    return epoch_loss, epoch_acc, info, model