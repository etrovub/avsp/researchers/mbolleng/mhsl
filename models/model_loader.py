from utils.utils import *
import torch


def load_model(model_name, net_params, device):
    net_params['device'] = device
    if model_name == 'HSL':
        from models.HSL.HSL import HSL
        model = HSL(net_params)
        view_model_param(model)
        return model.to(device)


def load_pretrained_model(model_name, ckpt_path, config_path, device):
    net_params = load_config(config_path)['net_params']
    net_params['device'] = device
    check_pt = torch.load(ckpt_path, map_location=torch.device('cpu'))
    model = load_model(model_name, net_params, device='cpu')
    model.load_state_dict(check_pt)
    return model.to(device)
