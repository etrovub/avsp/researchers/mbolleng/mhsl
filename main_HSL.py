import glob
import re
import signal
import threading
import time
import argparse
from data.Dataset import Dataset
from models.model_loader import load_model
from utils.utils import *
import torch.optim as optim
from torch.utils.data import DataLoader
from models.trainer import train_epoch, evaluate_network
from tqdm import tqdm
import wandb
from utils.utils import merge

def main():
    "------------------ for optimisation--------"
    config = load_config('./configs/HSL.yaml')
    parser = argparse.ArgumentParser()
    parser.add_argument('--init_lr', type=float)
    parser.add_argument('--weight_decay', type=float)
    parser.add_argument('--lr_schedule_patience', type=int)
    parser.add_argument('--no_change', type=int)
    parser.add_argument('--n_cluster', type=int)
    parser.add_argument('--hidden_dim', type=int)
    parser.add_argument('--dropout', type=float)
    parser.add_argument('--in_feat_dropout', type=float)
    parser.add_argument('--n_layer', type=int)
    parser.add_argument('--n_view', type=int)
    parser.add_argument('--batch_norm')
    parser.add_argument('--residual')
    parser.add_argument('--att', type=str)
    parser.add_argument('--fuse', type=str)
    parser.add_argument('--optimisation', default=config['params']['optimisation'])
    parser.add_argument('--k_split', default=config['data']['k_split'], type=int)
    parser.add_argument('--name', default=config['data']['name'], type=str)
    parser.add_argument('--gpu_id', default=None, type=int)
    parser.add_argument('--early', default=config['params']['early'], type=float)
    args = parser.parse_args()

    device = gpu_setup(config['gpu'], gpu_id=args.gpu_id)
    os.environ["WANDB_API_KEY"] = 'f6a1a7a209231118c99d5b6078fc26a2941ce3c3'
    wandb.login()
    wandb.init(project=config['model']+'_'+config['data']['name'], config=config)
    setting = config['data']['setting']
    k_split = args.k_split
    name = args.name
    dataset = Dataset(k_split=k_split, fully_connected=config['data']['fully_connected'],
                      name=name, setting=setting)
    params = config['params']
    net_params = config['net_params']
    net_params['name'] = name
    merge(config['params'], net_params)
    net_params['n_class'] = dataset.n_class
    net_params['n_modal'] = dataset.n_modal
    net_params['modal_name'] = dataset.modal_name
    net_params['modal_dim'] = dataset.modal_dim
    net_params['n_patient'] = dataset.n_patient
    seed_everything(params['seed'], device)
    avg_test_acc = []
    avg_train_acc = []
    avg_val_acc = []
    best_epoch_lst = []
    if params['cross_val']:
        split_number_lst = np.arange(k_split)
    else:
        split_number_lst = [params['split_num']]

    if args.optimisation:
        merge(vars(args), net_params)
        merge(vars(args), params)

    try:
        t0 = time.time()
        for split_number in split_number_lst:
            t0_split = time.time()
            trainset, valset, testset = dataset.train[split_number], dataset.val[split_number], dataset.test[split_number]
            model = load_model(config['model'], net_params, device)
            optimizer = optim.Adam(model.parameters(), lr=params['init_lr'], weight_decay=float(params['weight_decay']))
            scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min',
                                                             factor=float(params['lr_reduce_factor']),
                                                             patience=params['lr_schedule_patience'],
                                                             verbose=True, min_lr=float(params['min_lr']))
            train_loader = DataLoader(trainset, batch_size=params['batch_size'], shuffle=True,
                                      collate_fn=dataset.collate)
            val_loader = DataLoader(valset, batch_size=params['batch_size'], shuffle=False,
                                    collate_fn=dataset.collate)
            test_loader = DataLoader(testset, batch_size=params['batch_size'], shuffle=False,
                                     collate_fn=dataset.collate)
            print("RUN NUMBER: ", split_number)
            print("Training Graphs: ", len(trainset))
            print("Validation Graphs: ", len(valset))
            print("Test Graphs: ", len(testset))
            print("Number of Classes: ", net_params['n_class'])

            with tqdm(range(params['epochs'])) as t:
                for epoch in t:
                    t.set_description('Epoch %d' % epoch)
                    start = time.time()
                    train_epoch_loss, train_epoch_acc, optimizer, train_info, model = train_epoch(model, optimizer, device, train_loader, epoch, setting)
                    val_epoch_loss, val_epoch_acc, val_info, model = evaluate_network(model, device, val_loader, epoch, setting)
                    test_epoch_loss, test_epoch_acc, test_info, model = evaluate_network(model, device, test_loader, epoch, setting)

                    current_epoch = {'val_acc': val_epoch_acc,
                                     'val_loss': val_epoch_loss,
                                     'train_acc': train_epoch_acc,
                                     'train_loss': train_epoch_loss,
                                     'test_acc': test_epoch_acc,
                                     'test_loss': test_epoch_loss,
                                     'epoch': epoch}

                    run_epoch = {f" Fold {split_number} val_acc": val_epoch_acc,
                                 f" Fold {split_number} val_loss": val_epoch_loss,
                                 f" Fold {split_number} train_acc": train_epoch_acc,
                                 f" Fold {split_number} train_loss": train_epoch_loss,
                                 f" Fold {split_number} test_acc": test_epoch_acc,
                                 f" Fold {split_number} test_loss": test_epoch_loss,
                                 'epoch': epoch}
                    if not (args.optimisation):
                        if val_info is not None:
                            merge(val_info, run_epoch)
                        if params['tsne'] and epoch % params['print_epoch_interval'] == 0:
                            "--------TSNE-----------"
                            plt.figure()
                            sns.scatterplot(x='tsne_1', y='tsne_2', hue='label', style='modalities',
                                            data=model.get_tsne(),
                                            s=120, palette="deep")
                            plt.legend(bbox_to_anchor=(1, 1), loc=2)
                            plt.tight_layout()
                            run_epoch['tsne_label'] = wandb.Image(plt)
                            plt.close()
                            plt.figure()
                            g = sns.scatterplot(x='tsne_1', y='tsne_2', hue='patient', style='modalities',
                                            data=model.get_tsne(),
                                            s=120, palette="deep")
                            h, l = g.get_legend_handles_labels()
                            mod = np.where(np.array(l) == 'modalities')[0].item()
                            index = list(np.concatenate((np.arange(0, mod, 8), np.arange(mod, len(l)))))
                            plt.legend(list(np.array(h)[index]), list(np.array(l)[index]), bbox_to_anchor=(1, 1), loc=2)
                            plt.tight_layout()
                            run_epoch['tsne_patient'] = wandb.Image(plt)
                            plt.close()
                        if params['alignment_tab'] and epoch % params['print_epoch_interval'] == 0:
                            "-----------alignment tab----------"
                            run_epoch['alignment_table'] = wandb.Table(dataframe=model.get_alignment_mat())
                        if params['alignment_dict']:
                            "-----------alignment dict----------"
                            alignment_dict = model.get_alignment_dict()
                            merge(alignment_dict, run_epoch)
                        if epoch % params['print_epoch_interval'] == 0:
                                wandb.log(run_epoch)
                                wandb.watch(model)

                    if epoch == 0:
                        no_change = 0
                        save = True
                        best_epoch = current_epoch
                    else:
                        if not (current_epoch['val_acc'] > best_epoch['val_acc']):
                            no_change += 1
                            save = False
                        else:
                            best_epoch = current_epoch
                            no_change = 0
                            save = True

                    t.set_postfix(time=time.time() - start, lr=optimizer.param_groups[0]['lr'],
                                  train_loss=train_epoch_loss, val_loss=val_epoch_loss,
                                  train_acc=train_epoch_acc, val_acc=val_epoch_acc,
                                  test_acc=test_epoch_acc)

                    # Saving checkpoint
                    if not(args.optimisation):
                        if save:
                            ckpt_dir = os.path.join(config['path']['out_dir'], config['model'], "RUN_" + str(split_number))
                            if not os.path.exists(ckpt_dir):
                                os.makedirs(ckpt_dir)
                            torch.save(model.state_dict(), '{}.pkl'.format(ckpt_dir + "/epoch_" + str(epoch)))

                            files = glob.glob(ckpt_dir + '/*.pkl')
                            for file in files:
                                epoch_nb = file.split('_')[-1]
                                epoch_nb = int(epoch_nb.split('.')[0])
                                if epoch_nb < epoch - 1:
                                    os.remove(file)

                    scheduler.step(val_epoch_loss)
                    if no_change > params['no_change']:
                        print('Best epoch since since {} epochs '.format(str(no_change)))
                        break
                    if optimizer.param_groups[0]['lr'] <= float(params['min_lr']):
                        print('LR min')
                        break

                    # Stop training after params['max_time'] hours
                    if time.time() - t0_split > params[
                        'max_time'] * 3600 / 5:  # Dividing max_time by 5, since there are 5 runs
                        print('-' * 89)
                        print(
                            "Max_time for one train-val-test split experiment elapsed {:.3f} hours, so stopping".format(
                                params['max_time'] / 10))
                        break
                avg_val_acc.append(best_epoch['val_acc'])
                avg_test_acc.append(best_epoch['test_acc'])
                avg_train_acc.append(best_epoch['train_acc'])

                print("Test Accuracy [BEST EPOCH]: {:.4f}".format(best_epoch['test_acc'] * 100))
                print("Val Accuracy [BEST EPOCH]: {:.4f}".format(best_epoch['val_acc'] * 100))
                print("Train Accuracy [BEST EPOCH]: {:.4f}".format(best_epoch['train_acc'] * 100))
                print("BEST EPOCH: {}".format(best_epoch['epoch']))
            if args.optimisation:
                if best_epoch['val_acc'] <= params['early']:
                    break


    except KeyboardInterrupt:
        print('-' * 89)
        print('Exiting from training early because of KeyboardInterrupt')


    print("TOTAL TIME TAKEN: {:.4f}hrs".format((time.time() - t0) / 3600))
    #print("AVG TIME PER EPOCH: {:.4f}s".format(np.mean(per_epoch_time)))

    # Final test accuracy value averaged over k-fold
    print("""\n\n\nFINAL RESULTS\n\nTEST ACCURACY averaged: {:.4f} with s.d. {:.4f}""".format(
        np.mean(np.array(avg_test_acc)) * 100, np.std(avg_test_acc) * 100))
    print("\nAll splits Test Accuracies:\n", avg_test_acc)
    print("""\n\n\nFINAL RESULTS\n\nVAL ACCURACY averaged: {:.4f} with s.d. {:.4f}""".format(
        np.mean(np.array(avg_val_acc)) * 100, np.std(avg_val_acc) * 100))
    print("\nAll splits Train Accuracies:\n", avg_val_acc)
    print("""\n\n\nFINAL RESULTS\n\nTRAIN ACCURACY averaged: {:.4f} with s.d. {:.4f}""".format(
        np.mean(np.array(avg_train_acc)) * 100, np.std(avg_train_acc) * 100))
    print("\nAll splits Train Accuracies:\n", avg_train_acc)
    wandb.log(dict({'avg_val_acc': np.mean(np.array(avg_val_acc)),
                    'avg_train_acc': np.mean(np.array(avg_train_acc)),
                    "avg_test_acc": np.mean(np.array(avg_test_acc))}))

    #try_finish_wandb()
    #wandb.finish()

# This function forcibly kills the remaining wandb process.
def force_finish_wandb():

    #try:
        #with open('wandb/latest-run/logs/debug-internal.log', 'r') as f:
            #last_line = f.readlines()[-1]

    #except:
    with open(os.path.join(os.path.split(run.dir)[0], 'logs/debug-internal.log'), 'r') as f:
        last_line = f.readlines()[-1]

    match = re.search(r'(HandlerThread:|SenderThread:)\s*(\d+)', last_line)
    if match:
        pid = int(match.group(2))
        print(f'wandb pid: {pid}')
    else:

        print('Cannot find wandb process-id.')
        return

    try:
        os.kill(pid, signal.SIGKILL)
        print(f"Process with PID {pid} killed successfully.")
    except OSError:
        print(f"Failed to kill process with PID {pid}.")


# Start wandb.finish() and execute force_finish_wandb() after 60 seconds.
def try_finish_wandb():
    threading.Timer(60, force_finish_wandb).start()
    wandb.finish()

main()