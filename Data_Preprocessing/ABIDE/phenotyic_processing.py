import os
import pickle
import numpy as np
import pandas as pd
from scipy.sparse import csgraph
import csv
import gc

import numpy as np
import scipy.sparse as spsprs
from sklearn import preprocessing


data_folder = './data/ABIDE'

def get_ids(num_subjects=None):
    subject_IDs = np.genfromtxt(os.path.join(data_folder, 'subject_IDs.txt'), dtype=str)
    if num_subjects is not None:
        subject_IDs = subject_IDs[:num_subjects]

    return subject_IDs

from data.ABIDE.data_processing import process

def preprocess(n_feat_per_modal, init=None, train_idx=None):
    feat_256_1, _ = process(n_feat_per_modal, init=init, train_idx=train_idx)

    input_df = pd.read_csv('./data/ABIDE/ABIDE_pcp/Phenotypic_V1_0b_preprocessed1.csv',low_memory=False)
    subject_IDs = get_ids()
    subject_IDs = subject_IDs.astype('int')

    initial_data = input_df[input_df.SUB_ID.isin(subject_IDs)]
    data_age  = initial_data.AGE_AT_SCAN
    data_site = initial_data.SITE_ID
    data_eye = initial_data.EYE_STATUS_AT_SCAN
    initial_data.FIQ.isnull().sum()

    """for i in initial_data:
        print(i, '\t', initial_data[i].isnull().sum())"""


    anat = ['anat_cnr', 'anat_efc', 'anat_fber', 'anat_fwhm', 'anat_qi1', 'anat_snr']
    func = ['func_efc', 'func_fber', 'func_fwhm', 'func_dvars', 'func_outlier', 'func_quality', 'func_mean_fd', 'func_num_fd', 'func_perc_fd', 'func_gsr']

    data_func = initial_data[func]
    data_anat = initial_data[anat]

    data = initial_data.copy()

    min_age, max_age = data.AGE_AT_SCAN.min(), data.AGE_AT_SCAN.max()
    step, bins, block = 2, [min_age], min_age
    while block < max_age:
        block += 2
        bins.append(block)
    data.loc[:,'AGE_AT_SCAN'] = pd.cut(data.AGE_AT_SCAN, bins, right = False)

    data = pd.get_dummies(data, columns=['AGE_AT_SCAN'])
    data = pd.get_dummies(data, columns=['SITE_ID'])
    data = pd.get_dummies(data, columns=['SEX'])
    pheno_data = data[data.columns[data.columns.str.contains('^SITE|^AGE_AT_SCAN|^SEX')]]
    pheno_list = list(data.columns[data.columns.str.contains('^SITE|^AGE_AT_SCAN|^SEX')])
    anat_list  = anat
    func_list  = func
    feat_list  = pheno_list + anat_list + func_list

    pheno_num = len(pheno_list)
    anat_num  = len(anat)
    func_num  = len(func)

    ABIDE_modal_list = {'PHENO': pheno_list,
                        'ANAT' : anat_list,
                        'FUNC' : func_list}
    ABIDE_modal_num  = {'PHENO': pheno_num,
                        'ANAT' : anat_num,
                        'FUNC' : func_num}

    select_data = data[feat_list]
    standard_list = anat_list + func_list

    scaler        = preprocessing.StandardScaler()
    standard_data = scaler.fit_transform(data[standard_list])

    select_data[standard_list] = standard_data

    #np.save('./data/ABIDE/modal_feat_dict.npy',ABIDE_modal_list)
    #select_data.to_csv("./data/ABIDE/processed_data_modal_three.csv", index = False)
    #feat_256_1 = pd.read_csv('./data/ABIDE/processed_standard_data_256_1.csv', low_memory=False)
    pheno_256 = pd.read_csv('./data/ABIDE/processed_data_modal_three.csv', low_memory=False)
    feat_256_1_list = list(feat_256_1.columns)[:-1]
    ABIDE_modal_list['Correlation'] = feat_256_1_list
    data_256_1 = pd.concat([pheno_256, feat_256_1], axis=1)
    #np.save('./data/ABIDE/modal_feat_dict.npy',ABIDE_modal_list)
    #data_256_1.to_csv("./data/ABIDE/processed_data_modal.csv", index = False)

    return data_256_1, ABIDE_modal_list