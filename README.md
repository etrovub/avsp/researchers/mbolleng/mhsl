# Dynamic Multi-Hypergraph Strcture Learning for disease diagnosis

This project aims to create a Hypergraph-based model for patient diagnosis with multimodal data (See the link of the paper)

## How to run the code?

1. Install anaconda and create an environment with the file environement.yml

2. Run one the main file by choosing the configuration in the HSL.yaml file present in the config folder

## How to resimulate the results

To obtain the results of the paper, you can copy/paste the config present in final_config_[Dataset].yaml into the HSL.yaml file.

## How to get the data
The data presented here is pre-processed data from previous state-of-the art articles. To get the original data you can go to this link :
1. ABIDE : https://fcon_1000.projects.nitrc.org/indi/abide/
2. TADPOLE : https://adni.loni.usc.edu/tadpole-challenge-dataset-available/

If you want to use your own data, you have to provide :
1. a csv.file which contains multi-modal features, and
2. a multi-modal feature dict.
3. Then you need to create a class for your dataset following the same steps as ABIDE_dataset.py and TADPOLE_dataset.py and add your dataset to the Dataset.py class.
## Data preprocessing
The experimental set-up is the same as: https://github.com/SsGood/MMGL and https://github.com/parisots/population-gcn. 
Thanks a lot for the contribution to the community !

## How to incorporate your own model ?
As with data, you can incorporate your own model and all associated layers in the models/ folder and add it to model_loader.py. You can also design your own training method and add it to the folder.

## Hyparameters tuning

To search for hyperparameters, you will need to :
1. Create a WandB account on the site: https://wandb.ai/
2. In the config_optimization folder you will see how to set the hyperparameters search
3. Set os.environ["WANDB_API_KEY"] = [your API key]
4. Run in the temrinal : wandb sweep config_optimisation/sweep_[name].yaml

all the performance, log, checkpoint are save in the out folder to keep trace on your experiments

all the experiment tracking is also save on your wandb account

If you have any questions, please contact me on mbolleng@etrovub.be or maxime@bollengier.be